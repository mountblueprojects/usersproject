function problem1(users, idGiven) {
  if (Array.isArray(users) && typeof idGiven === "number") {
    for (let index = 0; index < users.length; index++) {
      if (users[index].id === idGiven) {
        return users[index];
      }
    }
  } else {
    return [];
  }

  return [];
}

module.exports = problem1;
