function problem6(users) {
  if (Array.isArray(users)) {
    const male = [];
    const female = [];
    const polyGender = [];
    const biGender = [];
    const genderQueer = [];
    const genderFluid = [];
    const agender = [];
    const genderArray = [
      male,
      female,
      polyGender,
      biGender,
      genderQueer,
      genderFluid,
      agender,
    ];

    for (let index = 0; index < users.length; index++) {
      switch (users[index].gender) {
        case "Male":
          male.push(users[index]);
          break;
        case "Female":
          female.push(users[index]);
          break;
        case "Polygender":
          polyGender.push(users[index]);
          break;
        case "Bigender":
          biGender.push(users[index]);
          break;
        case "Genderqueer":
          genderQueer.push(users[index]);
          break;
        case "Genderfluid":
          genderFluid.push(users[index]);
          break;
        case "Agender":
          agender.push(users[index]);
          break;
      }
    }

    return genderArray;
  } else {
    return [];
  }
}

module.exports = problem6;
