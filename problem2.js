function problem2(users) {
  if (Array.isArray(users)) {
    return users[users.length - 1];
  } else {
    return [];
  }
}

module.exports = problem2;
