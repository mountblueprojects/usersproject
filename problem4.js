function problem4(users) {
  if (Array.isArray(users)) {
    const emails = [];

    for (let index = 0; index < users.length; index++) {
      emails.push(users[index].email);
    }

    return emails;
  } else {
    return [];
  }
}

module.exports = problem4;
