function problem3(users) {
  if (Array.isArray(users)) {
    users.sort((a, b) => {
      return a.last_name.localeCompare(b.last_name);
    });

    return users;
  } else {
    return [];
  }
}

module.exports = problem3;
