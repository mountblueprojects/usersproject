function problem5(users) {
  if (Array.isArray(users)) {
    const newArray = [];

    for (let index = 0; index < users.length; index++) {
      if (users[index].gender === "Male") {
        newArray.push([
          `${users[index].first_name} ${users[index].last_name}`,
          users[index].email,
        ]);
      }
    }

    return newArray;
  } else {
    return [];
  }
}

module.exports = problem5;
